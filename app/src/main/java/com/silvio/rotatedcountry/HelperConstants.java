package com.silvio.rotatedcountry;

public class HelperConstants {

    public static String CONTINENT_EUROPE = "Europe";
    public static String CONTINENT_NORTH_AMERICA = "North America";
    public static String CONTINENT_SOUTH_AMERICA = "South America";
    public static String CONTINENT_ASIA = "Asia";
    public static String CONTINENT_AFRICA = "Africa";
    public static String CONTINENT_OCEANIA = "Oceania";

    public static String LEVEL_EASY = "Easy";
    public static String LEVEL_MEDIUM = "Medium";
    public static String LEVEL_HARD = "Hard";
    public static String LEVEL_EXPERT = "Expert";
}
