package com.silvio.rotatedcountry;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;

public class Guess extends AppCompatActivity implements GuessFragment.GuessInterface {

    private int correctAnswers = 0;
    private int totalAnswers = 0;
    private long totalTimeLeft = 0;
    public ArrayList<Country> usedCountries;
    private int currentLevel;
    private long totalScore;
    private int totalCA;
    private int totalTA;
    private long totalTL;
    private GuessFragment fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.empty_activity);

        currentLevel = getIntent().getIntExtra("currentLevel", 0);
        totalScore = getIntent().getLongExtra("totalScore", 0);
        totalCA = getIntent().getIntExtra("totalCA", 0);
        totalTA = getIntent().getIntExtra("totalTA", 0);
        totalTL = getIntent().getLongExtra("totalTL", 0);
        Log.d("LevelLOG", "current level guess= " + currentLevel);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragment = GuessFragment.newInstance(correctAnswers, totalAnswers, currentLevel);
        fragmentTransaction.add(R.id.fragment_container, fragment);
        fragmentTransaction.commitAllowingStateLoss();

        usedCountries = new ArrayList<>();

    }

    @Override
    public void onAnswerSelected(boolean isCorrect, Country country, long time) {
        totalAnswers++;
        if (isCorrect) correctAnswers++;
        usedCountries.add(country);
        Log.d("CountryLOG", "adding used country= " + country.getName());
        totalTimeLeft = totalTimeLeft + time;

        if (totalAnswers == 10) {
            Intent intent = new Intent(Guess.this, Score.class);
            intent.putExtra("correctAnswers", correctAnswers);
            intent.putExtra("totalAnswers", totalAnswers);
            intent.putExtra("totalTimeLeft", totalTimeLeft);
            intent.putExtra("currentLevel", currentLevel);
            intent.putExtra("totalScore", totalScore);
            intent.putExtra("totalCA", totalCA);
            intent.putExtra("totalTA", totalTA);
            intent.putExtra("totalTL", totalTL);
            startActivity(intent);
        }

        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            GuessFragment fragment = GuessFragment.newInstance(correctAnswers, totalAnswers, currentLevel);
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commitAllowingStateLoss();
            fragmentManager.popBackStack();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
