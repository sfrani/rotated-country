package com.silvio.rotatedcountry;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Score extends AppCompatActivity {

    private double multiplier;

    private long totalScore;
    private int totalCA;
    private int totalTA;
    private long totalTL;

  /* 15 answers
    private final int LEVEL_1_REQUIRED = 130000;
    private final int LEVEL_2_REQUIRED = 280000;
    private final int LEVEL_3_REQUIRED = 490000;
    private final int LEVEL_4_REQUIRED = 760000;
    private final int LEVEL_5_REQUIRED = 1100000;
    private final int LEVEL_6_REQUIRED = 1500000;
    private final int LEVEL_7_REQUIRED = 2000000; */

    private final int LEVEL_1_REQUIRED = 60000;
    private final int LEVEL_2_REQUIRED = 130000;
    private final int LEVEL_3_REQUIRED = 230000;
    private final int LEVEL_4_REQUIRED = 330000;
    private final int LEVEL_5_REQUIRED = 450000;
    private final int LEVEL_6_REQUIRED = 580000;
    private final int LEVEL_7_REQUIRED = 720000;

    private TextView tvLevel;
    private TextView score;
    private TextView answers;
    private Button playAgain;
    private Button save;
    private Button nextLevel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        final int correctAnswers = getIntent().getIntExtra("correctAnswers", 0);
        final int totalAnswers = getIntent().getIntExtra("totalAnswers", 0);
        final long totalTimeLeft = getIntent().getLongExtra("totalTimeLeft", 0);
        final int currentLevel = getIntent().getIntExtra("currentLevel", 1);
        Log.d("LevelLOG", "current level score= " + currentLevel);

        totalScore = getIntent().getLongExtra("totalScore", 0);
        totalCA = getIntent().getIntExtra("totalCA", 0) + correctAnswers;
        totalTA = getIntent().getIntExtra("totalTA", 0) + totalAnswers;
        totalTL = getIntent().getLongExtra("totalTL", 0) + totalTimeLeft;

        switch (currentLevel) {
            case 1:
                multiplier = 1;
                break;
            case 2:
                multiplier = 1.2;
                break;
            case 3:
                multiplier = 1.5;
                break;
            case 4:
                multiplier = 1.8;
                break;
            case 5:
                multiplier = 2;
                break;
            case 6:
                multiplier = 2.3;
                break;
            case 7:
                multiplier = 2.6;
                break;
            case 8:
                multiplier = 3;
                break;
        }

        final long levelScore = (long) ((totalTimeLeft * correctAnswers * multiplier) / 10);
        totalScore = totalScore + levelScore;

        tvLevel = (TextView) findViewById(R.id.tvLevel);
        answers = (TextView) findViewById(R.id.tvAnswers);
        TextView timeLeft = (TextView) findViewById(R.id.tvTimeLeft);
        score = (TextView) findViewById(R.id.tvScore);

        save = (Button) findViewById(R.id.btn_save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHandler databaseHandler = new DatabaseHandler(Score.this);
                databaseHandler.addScore(new ScoreObject(System.currentTimeMillis(), totalScore));
                Toast.makeText(Score.this, "Score saved", Toast.LENGTH_SHORT).show();
            }
        });

        playAgain = (Button) findViewById(R.id.btn_play_again);
        playAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Score.this, Guess.class);
                intent.putExtra("currentLevel", 1);
                startActivity(intent);
                finishAffinity();
            }
        });

        nextLevel = (Button) findViewById(R.id.btn_next_level);
        nextLevel.setText("Start level " + (currentLevel + 1));
        nextLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Score.this, Guess.class);
                intent.putExtra("currentLevel", (currentLevel + 1));
                intent.putExtra("totalScore", totalScore);
                intent.putExtra("totalCA", totalCA);
                intent.putExtra("totalTA", totalTA);
                intent.putExtra("totalTL", totalTL);
                startActivity(intent);
                finishAffinity();
            }
        });


        if (currentLevel < 8) {
            playAgain.setVisibility(View.GONE);
            save.setVisibility(View.GONE);
            nextLevel.setVisibility(View.VISIBLE);
            tvLevel.setText("Level " + currentLevel);
            score.setText("" + totalScore);
            answers.setText(totalCA + "/" + totalTA);
            timeLeft.setText("" + (totalTL / 1000) + "s");

            switch (currentLevel) {
                case 1:
                    if (totalScore < LEVEL_1_REQUIRED) gameOver(LEVEL_1_REQUIRED);
                    break;
                case 2:
                    if (totalScore < LEVEL_2_REQUIRED) gameOver(LEVEL_2_REQUIRED);
                    break;
                case 3:
                    if (totalScore < LEVEL_3_REQUIRED) gameOver(LEVEL_3_REQUIRED);
                    break;
                case 4:
                    if (totalScore < LEVEL_4_REQUIRED) gameOver(LEVEL_4_REQUIRED);
                    break;
                case 5:
                    if (totalScore < LEVEL_5_REQUIRED) gameOver(LEVEL_5_REQUIRED);
                    break;
                case 6:
                    if (totalScore < LEVEL_6_REQUIRED) gameOver(LEVEL_6_REQUIRED);
                    break;
                case 7:
                    if (totalScore < LEVEL_7_REQUIRED) gameOver(LEVEL_7_REQUIRED);
                    break;
            }
        } else {
            playAgain.setVisibility(View.VISIBLE);
            save.setVisibility(View.VISIBLE);
            nextLevel.setVisibility(View.GONE);
            tvLevel.setText("TOTAL");
            score.setText("" + totalScore);
            answers.setText(totalCA + "/" + totalTA);
            timeLeft.setText("" + (totalTL / 1000) + "s");
        }
    }

    private void gameOver(long requiredScore) {
        tvLevel.setText("GAME OVER");
        playAgain.setVisibility(View.VISIBLE);
        save.setVisibility(View.VISIBLE);
        nextLevel.setVisibility(View.GONE);
        TextView tvScoreDesc = (TextView) findViewById(R.id.tvScoreDesc);
        tvScoreDesc.setText("Score (" + requiredScore + " required)");
        score.setText("" + totalScore);
        answers.setText(totalCA + "/" + totalTA);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Score.this, Home.class);
        startActivity(intent);
        finishAffinity();
    }
}
