package com.silvio.rotatedcountry;

public class ScoreObject {

    public ScoreObject(long timestamp, long score){
        this.timestamp = timestamp;
        this.score = score;
    }

    private long timestamp;
    private long score;

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
