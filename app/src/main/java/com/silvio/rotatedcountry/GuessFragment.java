package com.silvio.rotatedcountry;

import android.content.ContentResolver;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;


public class GuessFragment extends Fragment {

    List<Country> countries;
    Country mainCountry;
    final int timeToAnswer = 10000;
    CountDownTimer countDownTimer;
    int correctAnswers;
    int totalAnswers;
    long timeLeft;
    private boolean isAnswered = false;
    private MediaPlayer correctSound;
    private MediaPlayer wrongSound;
    private boolean isSoundOn;
    private int currentLevel;
    private ImageView image;

    private Button answer1;
    private Button answer2;
    private Button answer3;
    private Button answer4;


    private List<Country> dbList;

    public interface GuessInterface {
        void onAnswerSelected(boolean isCorrect, Country mainCountry, long time);
    }

    public static GuessFragment newInstance(int correctAnswers, int totalAnswers, int currentLevel) {
        Bundle args = new Bundle();
        args.putInt("correctAnswers", correctAnswers);
        args.putInt("totalAnswers", totalAnswers);
        args.putInt("currentLevel", currentLevel);
        GuessFragment fragment = new GuessFragment();
        fragment.setArguments(args);
        return fragment;
    }

  /*  @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(image != null) image.setImageDrawable(null);
    } */

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        correctAnswers = getArguments().getInt("correctAnswers");
        totalAnswers = getArguments().getInt("totalAnswers");
        currentLevel = getArguments().getInt("currentLevel");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_guess, container, false);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        isSoundOn = prefs.getBoolean("sound", true);

        image = (ImageView) view.findViewById(R.id.image);
        answer1 = (Button) view.findViewById(R.id.answer1);
        answer2 = (Button) view.findViewById(R.id.answer2);
        answer3 = (Button) view.findViewById(R.id.answer3);
        answer4 = (Button) view.findViewById(R.id.answer4);
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        final TextView score = (TextView) view.findViewById(R.id.tvCorrectAnswers);
        progressBar.setMax(timeToAnswer);
        progressBar.setProgress(timeToAnswer);

        score.setText(correctAnswers + "/" + totalAnswers);

        DatabaseHandler databaseHandler = new DatabaseHandler(getContext());
        dbList = databaseHandler.getAllCountries();

        switch (currentLevel) {
            case 1:
                countries = new ArrayList<>();
                for (int i = 0; i < dbList.size(); i++) {
                    if (dbList.get(i).getLevel().equals(HelperConstants.LEVEL_EASY)) {
                        countries.add(dbList.get(i));
                    }
                }
                Log.d("SizeLOG", "easy size= " + countries.size());
                break;
            case 2:
                countries = new ArrayList<>();
                for (int i = 0; i < dbList.size(); i++) {
                    if (dbList.get(i).getLevel().equals(HelperConstants.LEVEL_MEDIUM)) {
                        countries.add(dbList.get(i));
                    }
                }
                Log.d("SizeLOG", "medium size= " + countries.size());
                break;
            case 3:
                countries = new ArrayList<>();
                for (int i = 0; i < dbList.size(); i++) {
                    if (dbList.get(i).getLevel().equals(HelperConstants.LEVEL_EASY)) {
                        countries.add(dbList.get(i));
                    }
                }
                break;
            case 4:
                countries = new ArrayList<>();
                for (int i = 0; i < dbList.size(); i++) {
                    if (dbList.get(i).getLevel().equals(HelperConstants.LEVEL_MEDIUM)) {
                        countries.add(dbList.get(i));
                    }
                }
                break;
            case 5:
                countries = new ArrayList<>();
                for (int i = 0; i < dbList.size(); i++) {
                    if (dbList.get(i).getLevel().equals(HelperConstants.LEVEL_HARD)) {
                        countries.add(dbList.get(i));
                    }
                }
                Log.d("SizeLOG", "hard size= " + countries.size());
                break;
            case 6:
                countries = new ArrayList<>();
                for (int i = 0; i < dbList.size(); i++) {
                    if (dbList.get(i).getLevel().equals(HelperConstants.LEVEL_EXPERT)) {
                        countries.add(dbList.get(i));
                    }
                }
                Log.d("SizeLOG", "expert size= " + countries.size());
                break;
            case 7:
                countries = new ArrayList<>();
                for (int i = 0; i < dbList.size(); i++) {
                    if (dbList.get(i).getLevel().equals(HelperConstants.LEVEL_HARD)) {
                        countries.add(dbList.get(i));
                    }
                }
                break;
            case 8:
                countries = new ArrayList<>();
                for (int i = 0; i < dbList.size(); i++) {
                    if (dbList.get(i).getLevel().equals(HelperConstants.LEVEL_EXPERT)) {
                        countries.add(dbList.get(i));
                    }
                }
                break;
        }

        removeUsedCountries();
        if (countries.size() < 4) countries = databaseHandler.getAllCountries();
        Collections.shuffle(countries);
        answer1.setText(countries.get(0).getName());
        answer2.setText(countries.get(1).getName());
        answer3.setText(countries.get(2).getName());
        answer4.setText(countries.get(3).getName());

        final int min = 0;
        final int max = 3;
        Random random = new Random();
        int mainCountryNumber = random.nextInt((max - min) + 1) + min;
        mainCountry = countries.get(mainCountryNumber);

        String mDrawableName = countries.get(mainCountryNumber).getImage();
       /* int resID = getResources().getIdentifier(mDrawableName, "drawable", getContext().getPackageName());
        image.setImageResource(resID); */

        Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/drawable/" + mDrawableName);
        Picasso.with(getContext())
                .load(uri)
                .resize(250, 250)
                .centerInside()
                .into(image);

        if (currentLevel == 3 || currentLevel == 4 || currentLevel == 7 || currentLevel == 8) {
            image.setRotation((float) random.nextInt((360) + 1) + 0);
        }

        correctSound = MediaPlayer.create(getContext(), R.raw.correct_answer);
        wrongSound = MediaPlayer.create(getContext(), R.raw.wrong_answer);

        answer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isAnswered) checkAnswer(answer1);
            }
        });
        answer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isAnswered) checkAnswer(answer2);
            }
        });
        answer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isAnswered) checkAnswer(answer3);
            }
        });
        answer4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isAnswered) checkAnswer(answer4);
            }
        });

        countDownTimer = new CountDownTimer(timeToAnswer, 100) {
            public void onTick(long millisUntilFinished) {
                progressBar.setProgress((int) millisUntilFinished);
                timeLeft = millisUntilFinished;
            }

            public void onFinish() {
                try {
                    if (!isAnswered) {
                        GuessInterface listener = (GuessInterface) getContext();
                        if (listener != null) listener.onAnswerSelected(false, mainCountry, 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        countDownTimer.start();

        return view;
    }

    private void checkAnswer(Button button) {
        if (button.getText().equals(mainCountry.getName())) {
            button.setBackgroundColor(Color.GREEN);
            if (isSoundOn) correctSound.start();
            notifyAnswer(true);
        } else {
            button.setBackgroundColor(Color.RED);
            if (isSoundOn) wrongSound.start();
            notifyAnswer(false);
        }
    }

    private void removeUsedCountries() {
        ArrayList<Country> check = ((Guess) (getActivity())).usedCountries;
        Log.d("CountryLOG", "used countries size= " + check.size());
        Log.d("CountryLOG", "countries = " + countries);
        for (int i = 0; i < check.size(); i++) {
            for (Iterator<Country> iter = countries.iterator(); iter.hasNext(); ) {
                Country element = iter.next();
                if (element.getName().equals(check.get(i).getName())) {
                    iter.remove();
                    Log.d("CountryLOG", "removing " + check.get(i).getName());
                }
            }
        }
    }

    private void notifyAnswer(final boolean isCorrect) {
        isAnswered = true;
        countDownTimer.cancel();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
              //  recycleDefaultImage();
                GuessInterface listener = (GuessInterface) getContext();
                if (listener != null) listener.onAnswerSelected(isCorrect, mainCountry, timeLeft);
                correctSound.stop();
                wrongSound.stop();
            }
        }, 1000);
    }

    protected void recycleDefaultImage() {
      /*  Drawable imageDrawable = image.getDrawable();
        image.setImageDrawable(null); //this is necessary to prevent getting Canvas: can not draw recycled bitmap exception

        if (imageDrawable != null && imageDrawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = ((BitmapDrawable) imageDrawable);

            if (!bitmapDrawable.getBitmap().isRecycled()) {
                bitmapDrawable.getBitmap().recycle();
            }
        } */
      try {
          ((BitmapDrawable) image.getDrawable()).getBitmap().recycle();
      } catch (Exception e){
          e.printStackTrace();
      }
    }
}
