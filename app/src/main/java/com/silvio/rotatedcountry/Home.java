package com.silvio.rotatedcountry;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

       // fillDatabase();
        new AsyncFillDatabase().execute();

        Button startButton = (Button) findViewById(R.id.btn_start);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, Guess.class);
                intent.putExtra("currentLevel", 1);
                startActivity(intent);
            }
        });

        Button historyButton = (Button) findViewById(R.id.btn_history);
        historyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, HighScores.class);
                startActivity(intent);
            }
        });

        Button settingsButton = (Button) findViewById(R.id.btn_settings);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, Settings.class);
                startActivity(intent);
            }
        });
    }

    private void fillDatabase() {
        DatabaseHandler databaseHandler = new DatabaseHandler(this);
        if (databaseHandler.getAllCountries().size() == 0) {
            databaseHandler.addCountry(new Country("Albania", "albania", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Algeria", "algeria", HelperConstants.CONTINENT_AFRICA, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Angola", "angola", HelperConstants.CONTINENT_AFRICA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Argentina", "argentina", HelperConstants.CONTINENT_SOUTH_AMERICA, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Australia", "australia", HelperConstants.CONTINENT_OCEANIA, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Austria", "austria", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Bangladesh", "bangladesh", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Belarus", "belarus", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Belgium", "belgium", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Bolivia", "bolivia", HelperConstants.CONTINENT_SOUTH_AMERICA, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Brazil", "brazil", HelperConstants.CONTINENT_SOUTH_AMERICA, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Cameroon", "cameroon", HelperConstants.CONTINENT_AFRICA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Canada", "canada", HelperConstants.CONTINENT_NORTH_AMERICA, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Chile", "chile", HelperConstants.CONTINENT_SOUTH_AMERICA, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Chad", "chad", HelperConstants.CONTINENT_AFRICA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("China", "china", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Colombia", "colombia", HelperConstants.CONTINENT_SOUTH_AMERICA, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Croatia", "croatia", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Czech Republic", "czech", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Denmark", "denmark", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("DR Congo", "congo", HelperConstants.CONTINENT_AFRICA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Ecuador", "ecuador", HelperConstants.CONTINENT_SOUTH_AMERICA, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Egypt", "egypt", HelperConstants.CONTINENT_AFRICA, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Estonia", "estonia", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Finland", "finland", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("France", "france", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Germany", "germany", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Greece", "greece", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Hungary", "hungary", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Iceland", "iceland", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("India", "india", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Iran", "iran", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Iraq", "iraq", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Ireland", "ireland", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Italy", "italy", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Japan", "japan", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Kazakhstan", "kazakhstan", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Latvia", "latvia", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Liberia", "liberia", HelperConstants.CONTINENT_AFRICA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Libya", "libya", HelperConstants.CONTINENT_AFRICA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Lithuania", "lithuania", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Macedonia", "macedonia", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Madagascar", "madagascar", HelperConstants.CONTINENT_AFRICA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Mali", "mali", HelperConstants.CONTINENT_AFRICA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Mexico", "mexico", HelperConstants.CONTINENT_SOUTH_AMERICA, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Mongolia", "mongolia", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Montenegro", "montenegro", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Morocco", "morocco", HelperConstants.CONTINENT_AFRICA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Nepal", "nepal", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Netherlands", "netherlands", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("New Zealand", "new_zealand", HelperConstants.CONTINENT_OCEANIA, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Nigeria", "nigeria", HelperConstants.CONTINENT_AFRICA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Norway", "norway", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("North Korea", "north_korea", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Paraguay", "paraguay", HelperConstants.CONTINENT_SOUTH_AMERICA, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Peru", "peru", HelperConstants.CONTINENT_SOUTH_AMERICA, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Poland", "poland", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Portugal", "portugal", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Romania", "romania", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Russia", "russia", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Saudi Arabia", "saudi_arabia", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Senegal", "senegal", HelperConstants.CONTINENT_AFRICA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Serbia", "serbia", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Slovakia", "slovakia", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Slovenia", "slovenia", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("South Korea", "south_korea", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Spain", "spain", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Sudan", "sudan", HelperConstants.CONTINENT_AFRICA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Syria", "syria", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Sweden", "sweden", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Switzerland", "switzerland", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Ukraine", "ukraine", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("United Kingdom", "uk", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Uruguay", "uruguay", HelperConstants.CONTINENT_SOUTH_AMERICA, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("USA", "usa", HelperConstants.CONTINENT_NORTH_AMERICA, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Thailand", "thailand", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Tunisia", "tunisia", HelperConstants.CONTINENT_AFRICA, HelperConstants.LEVEL_HARD));
            databaseHandler.addCountry(new Country("Turkey", "turkey", HelperConstants.CONTINENT_EUROPE, HelperConstants.LEVEL_EASY));
            databaseHandler.addCountry(new Country("Turkmenistan", "turkmenistan", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Venezuela", "venezuela", HelperConstants.CONTINENT_SOUTH_AMERICA, HelperConstants.LEVEL_MEDIUM));
            databaseHandler.addCountry(new Country("Vietnam", "vietnam", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_EXPERT));
            databaseHandler.addCountry(new Country("Yemen", "yemen", HelperConstants.CONTINENT_ASIA, HelperConstants.LEVEL_EXPERT));
        }
    }

    private class AsyncFillDatabase extends AsyncTask<String, Integer, String>{
        @Override
        protected String doInBackground(String... params) {
            fillDatabase();
            return null;
        }
    }
}
