package com.silvio.rotatedcountry;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class HighScores extends AppCompatActivity{

    private ListView listView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscores);

        DatabaseHandler databaseHandler = new DatabaseHandler(this);
        ArrayList<ScoreObject> scoreItems = new ArrayList<>(databaseHandler.getAllScores());

        Collections.sort(scoreItems, new Comparator<ScoreObject>(){
            public int compare(ScoreObject obj1, ScoreObject obj2) {
                 return Long.valueOf(obj2.getScore()).compareTo(obj1.getScore());
            }
        });

        listView = (ListView) findViewById(R.id.listView);
        HighScoreListAdapter adapter = new HighScoreListAdapter(this, scoreItems);
        listView.setAdapter(adapter);

        Button clearScores = (Button) findViewById(R.id.btn_clear_highscores);
        clearScores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHandler databaseHandler = new DatabaseHandler(HighScores.this);
                databaseHandler.deleteAllScores();
                listView.setAdapter(null);
            }
        });
    }
}
