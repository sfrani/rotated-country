package com.silvio.rotatedcountry;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "countryDatabase";

    // Contacts table name
    private static final String TABLE_COUNTRIES = "countries";
    private static final String TABLE_SCORES = "scores";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_CONTINENT = "continent";
    private static final String KEY_LEVEL = "level";

    private static final String KEY_TIMESTAMP = "timestamp";
    private static final String KEY_SCORE = "score";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_COUNTRIES_TABLE = "CREATE TABLE " + TABLE_COUNTRIES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_IMAGE + " TEXT," + KEY_CONTINENT + " TEXT," + KEY_LEVEL + " TEXT" + ")";
        db.execSQL(CREATE_COUNTRIES_TABLE);

        String CREATE_SCORES_TABLE = "CREATE TABLE " + TABLE_SCORES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TIMESTAMP + " INTEGER,"
                + KEY_SCORE + " INTEGER" + ")";
        db.execSQL(CREATE_SCORES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COUNTRIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCORES);
        onCreate(db);
    }

    void addCountry(Country country) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, country.getName());
        values.put(KEY_IMAGE, country.getImage());
        values.put(KEY_CONTINENT, country.getContinent());
        values.put(KEY_LEVEL, country.getLevel());
        db.insert(TABLE_COUNTRIES, null, values);
        db.close();
    }

    void addScore(ScoreObject scoreObject) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TIMESTAMP, scoreObject.getTimestamp());
        values.put(KEY_SCORE, scoreObject.getScore());
        db.insert(TABLE_SCORES, null, values);
        db.close();
    }


    public List<Country> getAllCountries() {
        List<Country> countryList = new ArrayList<Country>();
        String selectQuery = "SELECT  * FROM " + TABLE_COUNTRIES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Country country = new Country();
                country.setID(Integer.parseInt(cursor.getString(0)));
                country.setName(cursor.getString(1));
                country.setImage(cursor.getString(2));
                country.setContinent(cursor.getString(3));
                country.setLevel(cursor.getString(4));
                countryList.add(country);
            } while (cursor.moveToNext());
        }
        return countryList;
    }

    public List<ScoreObject> getAllScores() {
        List<ScoreObject> scoreList = new ArrayList<ScoreObject>();
        String selectQuery = "SELECT  * FROM " + TABLE_SCORES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                ScoreObject scoreObject = new ScoreObject(Long.parseLong(cursor.getString(1)), Long.parseLong(cursor.getString(2)));
                scoreList.add(scoreObject);
            } while (cursor.moveToNext());
        }
        return scoreList;
    }

    public void deleteAllScores() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SCORES, null, null);
        db.close();
    }


}
