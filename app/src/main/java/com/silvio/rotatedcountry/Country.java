package com.silvio.rotatedcountry;

public class Country {

    private int id;
    private String name;
    private String image;
    private String continent;
    private String level;

    public Country(String name, String image, String continent, String level){
        this.name = name;
        this.image = image;
        this.continent = continent;
        this.level = level;
    }

    public Country(){};

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
