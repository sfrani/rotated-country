package com.silvio.rotatedcountry;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class HighScoreListAdapter extends ArrayAdapter<ScoreObject> {

    Context context;
    ArrayList<ScoreObject> scoreItems;
    ViewHolder holder;
    View rowView;

    public HighScoreListAdapter(Context context, ArrayList<ScoreObject> scoreItems) {
        super(context, R.layout.highscore_row, scoreItems);

        this.context = context;
        this.scoreItems = new ArrayList<>(scoreItems);
    }

    public View getView(int position, View view, ViewGroup parent) {
        rowView = view;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.highscore_row, null, true);
            holder = new ViewHolder();

            holder.position = (TextView) rowView.findViewById(R.id.tvPosition);
            holder.date = (TextView) rowView.findViewById(R.id.tvDate);
            holder.score = (TextView) rowView.findViewById(R.id.tvScore);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.date.setText("" + convertToDate(scoreItems.get(position).getTimestamp()));
        holder.score.setText("" + scoreItems.get(position).getScore());
        holder.position.setText("" + (position + 1) + ".");
        return rowView;

    }

    static class ViewHolder {
        TextView position;
        TextView date;
        TextView score;
    }

    private String convertToDate(long timestamp){
        return new SimpleDateFormat("dd.MM.yyyy. HH:mm", Locale.US).format(new Date(timestamp));
    }
}
